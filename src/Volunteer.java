public class Volunteer extends GuestServices
{
	int scheduledLocation; //1-8: Exhibits, 9: Gift Shop, 10: Data entry
	
	public Volunteer()
	{
		scheduledLocation = 0;
	}//Volunteer

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay, 
						int scheduledLocation)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.pay = pay;
		this.scheduledLocation = scheduledLocation;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public void work(int numHours, int location)
	{
		scheduledLocation = location;
		
		System.out.print(name + " has volunteered for " + numHours + " hours at ");
		switch(location)
		{
			case 1: System.out.println("the Minnesota Trail.");
				break;
			case 2: System.out.println("the Tropics Trail.");
				break;
			case 3: System.out.println("the Russia's Grizzly Coast.");
				break;
			case 4: System.out.println("the Northern Trail.");
				break;
			case 5: System.out.println("the Discovery Bay.");
				break;
			case 6: System.out.println("the Family Farm.");
				break;
			case 7: System.out.println("the South Entry.");
				break;
			case 8: System.out.println("the Bird Show.");
				break;
			case 9: System.out.println("the Gift Shop.");
				break;
			case 10: System.out.println("data entry.");
				break;
		}
		
		hoursWorked = hoursWorked + numHours;
		System.out.println("Total hours volunteer: " + hoursWorked);
	}
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		
		string+= "Scheduled location: ";
		switch(scheduledLocation)
		{
			case 1: string+= "Minnesota Trail\n";
				break;
			case 2: string+= "Tropics Trail\n";
				break;
			case 3: string+= "Russia's Grizzly Coast\n";
				break;
			case 4: string+= "Northern Trail\n";
				break;
			case 5: string+= "Discovery Bay\n";
				break;
			case 6: string+= "Family Farm\n";
				break;
			case 7: string+= "South Entry\n";
				break;
			case 8: string+= "Bird Show\n";
				break;
			case 9: string+= "Gift Shop\n";
				break;
			case 10: string+= "Data Entry\n";
				break;
			default: string+="Unscheduled/Unknown location\n";
				break;
		}
		
		return string;
	} //toString
}//class