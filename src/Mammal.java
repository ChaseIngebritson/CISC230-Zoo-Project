public class Mammal extends Animal
{
	boolean flight;
	boolean nocturnal;
	boolean tail;
	boolean horns;
	
	public Mammal()
	{
		flight = false;
		nocturnal = false;
		tail = false;
		horns = false;
	} //Mammal
	
	public void populate(String name, String origin, String habitat, int exhibit, int diet, int conserveStatus, String food,
							boolean flight, boolean nocturnal, boolean tail, boolean horns)
	{
		this.name = name;
		this.origin = origin;
		this.habitat = habitat;
		this.exhibit = exhibit;
		this.diet = diet;
		this.conserveStatus = conserveStatus;
		this.food = food;
		this.flight = flight;
		this.nocturnal = nocturnal;
		this.tail = tail;
		this.horns = horns;
		
		System.out.println("(Mammal) " + name + " has been populated");
	} //populate
	
	public boolean getFlight() { return flight; }
	public boolean getNocturnal() { return nocturnal; }
	public boolean getTail() { return tail; }
	public boolean getHorns() { return horns; }
	
	public String toString()
	{
		String string = "";
		
		string+= "Name: " + name + "\n";
		string+= "Origin: " + origin + "\n";
		string+= "Habitat: " + habitat + "\n";
		
		string+= "Exhibit: ";
		switch (exhibit)
		{
		case 1: string+= "Minnesota Trail\n";
			break;
		case 2: string+= "Tropics Trail\n";
			break;
		case 3: string+= "Russia's Grizzly Coast\n";
			break;
		case 4: string+= "Northern Trail\n";
			break;
		case 5: string+= "Discovery Bay\n";
			break;
		case 6: string+= "Family Farm\n";
			break;
		case 7: string+= "South Entry\n";
			break;
		case 8: string+= "Bird Show\n";
			break;
	}
		
		string+= "Diet: " + diet + "\n";
		
		string+= "Conservation Status: ";
		switch (conserveStatus)
		{
			case 1: string+= "Least Concern (LC)\n";
				break;
			case 2: string+= "Near Threatened (NT)\n";
				break;
			case 3: string+= "Vulnerable (VU)\n";
				break;
			case 4: string+= "Endangered (EN)\n";
				break;
			case 5: string+= "Critically Engdangered (CR)\n";
				break;
			case 6: string+= "Extinct in the Wild (EW)\n";
				break;
			case 7: string+= "Extinct (EX)\n";
				break;
		}
		
		string+= "Food: " + food + "\n";
		
		string+= "Flight: ";
		if (flight) {
			string+= "Yes\n";
		} else {
			string+= "No\n";
		}
		
		string+= "Nocturnal: ";
		if (nocturnal) {
			string+= "Yes\n";
		} else {
			string+= "No\n";
		}
		
		string+= "Tail: ";
		if (tail) {
			string+= "Present\n";
		} else {
			string+= "Not Present\n";
		}
		
		string+= "Horns: ";
		if (horns) {
			string+= "Present\n";
		} else {
			string+= "Not Present\n";
		}
		
		return string;
	} //toString
}
