import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JFileChooser;

//This class will act as a container for main() and the highest level
public class MNZoo
{
	
	/*
	 * In order to use the file chooser:
	 * First start the application
	 * Then select all four animal files
	 * Finally choose "Open" on the bottom right
	 * 
	 * This will automatically populate all animal objects
	 */
	
	public static Scanner keyboard = new Scanner(System.in);
	
	//Create Animal object arrays
	private static Mammal[] mammals = new Mammal[12];
	private static Bird[] birds = new Bird[2];
	private static AmphibianReptile[] ars = new AmphibianReptile[3];
	private static Fish[] fish = new Fish[3];
	
	//Create an array to hold the Animal object arrays
	private static Animal[][] animals = {mammals, birds, ars, fish};
	
	//Create Employee object array
	private static Employee[] employees = new Employee[15];
			
	public static void main(String[] args)
	{		
		//Initialize Animal objects to put in arrays
		for (int i=0; i < mammals.length; i++)
		{
			mammals[i] = new Mammal();
		}
		for (int i=0; i < birds.length; i++)
		{
			birds[i] = new Bird();
		}
		for (int i=0; i < ars.length; i++)
		{
			ars[i] = new AmphibianReptile();
		}
		for (int i=0; i < fish.length; i++)
		{
			fish[i] = new Fish();
		}
		
		//Reads the files
		String[][][] files = readFiles();
		
		//Processes the files
		processFiles(files);
		
		//Begins user prompt
		promptUser();
	}//main
	
	//This will open files, read through them, and return a three dimensional array with their contents
	private static String[][][] readFiles()
	{	
		//Creates file chooser
		final JFileChooser fileChooser = new JFileChooser();
		//Allows the ability to select multiple files in the file chooser
		fileChooser.setMultiSelectionEnabled(true);
		//Sets the title of the dialog box
		fileChooser.setDialogTitle("Choose All Animal and Employee Files");
		//Opens the file chooser
		fileChooser.showOpenDialog(fileChooser);
		//Pulls the selected files and puts them in an array
		File[] files = fileChooser.getSelectedFiles();
		
		//First dimension: A file
		//Second dimension: An animal/employee
		//Third dimension: An animal/employee's attribute
		String[][][] returnArray = new String[files.length][20][20];
		
		for (int i=0; i < files.length; i++)
		{
			//Assigns the name of the file to the first place in the array to be able to tell the arrays apart
			returnArray[i][0][0] = files[i].getName();
	
			String line;	
			try
			{
				Scanner inFile = new Scanner(files[i]);
				
				int j = 1;
				while (inFile.hasNext())
				{
					//Iterate through the lines in the file
					line = inFile.nextLine();
			
					//Add the split up line to the return array
					returnArray[i][j] = line.split(",");
					
					j++;
				} //while
				
				//Close the scanner
				inFile.close();
				System.out.println(files[i].getName() + " has been read.");
			} catch (FileNotFoundException e) { System.out.println("File not found: " + e); }
		}//for (int i=0; i < files.length; i++)
		
		//Send back the array
		return returnArray;
	}//readFile

	//This will take the three dimensional array that readFile() produces and use it to populate the objects
	private static void processFiles(String[][][] files)
	{
		for (int currentFile=0; currentFile < files.length; currentFile++)
		{
			//Iterate through currentFile
			int animal = 1;
			while (files[currentFile][animal][0] != null)
			{
				//Create an array of Objects to hold the parsed information from currentFile
				Object[] parsedFile = new Object[files[currentFile][animal].length];
				
				//Iterate though the second degree array in currentFile
				for (int attribute=0; attribute < files[currentFile][animal].length; attribute++)
				{
					//Check if the attribute can be changed to an int
					try {
						parsedFile[attribute] = Integer.parseInt(files[currentFile][animal][attribute]);
					} catch (NumberFormatException e) {
						//Check if the attribute can be changed to a double
						try {
							parsedFile[attribute] = Double.parseDouble(files[currentFile][animal][attribute]);
						} catch (NumberFormatException f) {
							//Check if the attribute can be changed to a boolean
							if (files[currentFile][animal][attribute].equalsIgnoreCase("true"))	{
								parsedFile[attribute] = true;
							} else if (files[currentFile][animal][attribute].equalsIgnoreCase("false")) {
								parsedFile[attribute] = false;
							} else {
								//Add the string attribute to the new array if all else fails
								parsedFile[attribute] = files[currentFile][animal][attribute];
							}//else
						}//catch (NumberFormatException f)
					}//catch (NumberFormatException e)
				}//for (int attribute=0; attribute < files[currentFile][animal].length; attribute++)
				
				//Checks what file is being read and populates accordingly
				//For animals
				if (files[currentFile][0][0].equalsIgnoreCase("Mammals.txt")) {
					mammals[animal-1].populate((String)parsedFile[0], (String)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (String)parsedFile[6], (boolean)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9], (boolean)parsedFile[10]);
				} else if (files[currentFile][0][0].equalsIgnoreCase("Birds.txt")) {
					birds[animal-1].populate((String)parsedFile[0], (String)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (String)parsedFile[6], (int)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9]);
				} else if (files[currentFile][0][0].equalsIgnoreCase("AmphibianReptiles.txt")) {
					ars[animal-1].populate((String)parsedFile[0], (String)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (String)parsedFile[6], (boolean)parsedFile[7]);
				} else if (files[currentFile][0][0].equalsIgnoreCase("Fish.txt")) {
					fish[animal-1].populate((String)parsedFile[0], (String)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (String)parsedFile[6], (String)parsedFile[7]);
				//For employees
				} else if (files[currentFile][0][0].equalsIgnoreCase("Employees.txt")) {
					if (files[currentFile][animal][2].equalsIgnoreCase("Zoo Manager")) {
						employees[animal-1] = new ZooManager();
						((ZooManager)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9], (boolean)parsedFile[10]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Zookeeper")) {
						employees[animal-1] = new Zookeeper();
						((Zookeeper)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9], (boolean)parsedFile[10]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Zoologist")) {
						employees[animal-1] = new Zoologist();
						((Zoologist)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9], (boolean)parsedFile[10]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Veterinarian")) {
						employees[animal-1] = new Vet();
						((Vet)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Veterinarian Technician")) {
						employees[animal-1] = new VetTech();
						((VetTech)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (boolean)parsedFile[8], (boolean)parsedFile[9]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Guest Services Manager")) {
						employees[animal-1] = new GSManager();
						((GSManager)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Guest Services Employee")) {
						employees[animal-1] = new GSEmployee();
						((GSEmployee)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (int)parsedFile[8]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Volunteer Manager")) {
						employees[animal-1] = new VolManager();
						((VolManager)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7]);
					} else if (files[currentFile][animal][2].equalsIgnoreCase("Volunteer")) {
						employees[animal-1] = new Volunteer();
						((Volunteer)employees[animal-1]).populate((String)parsedFile[0], (int)parsedFile[1], (String)parsedFile[2], (int)parsedFile[3], (int)parsedFile[4], (int)parsedFile[5], (int)parsedFile[6], (double)parsedFile[7], (int)parsedFile[8]);
					}
				} else {
					System.out.println("Unknown file, ignoring.");
				} //else
				
				animal++;
			}//while (files[currentFile][animal][0] != null)
		}//for (int currentFile=0; currentFile < files.length; currentFile++)
	}//processFile
	
	//Allows the user to input commands
	private static void promptUser()
	{
		int intInput;
		int intInput2;
		String stringInput;
		String stringInput2;
		Animal animal = null;
		Employee employee = null;
		Employee employee2 = null;
		
		//Begin level 1 prompt
		while(true)
		{
			System.out.println("What would you like to do?");
			System.out.println("\tInteract with the animals: 1");
			System.out.println("\tInteract with the employees: 2");
			System.out.println("\tEnd: 0");
			intInput = keyboard.nextInt();
			
			//Level 1 - Choice 1: Interact with the animals
			if (intInput == 1) {
				//Begin level 2 prompt
				while(true)
				{
					System.out.println("How would you like to interact with the animals?");
					System.out.println("\tDisplay animal information: 1");
					System.out.println("\tPet animal: 2");
					System.out.println("\tDevelop an enrichment using a Zoologist: 3");
					System.out.println("\tPerform examination: 4");
					System.out.println("\tBack: 0");
					intInput = keyboard.nextInt();
				
					//Level 2 - Choice 0: Back to level 1 prompt 
					if (intInput == 0) {
						break;
					}
					
					//Select what animal to interact with
					System.out.println("What animal would you like to interact with?");
					System.out.println("Enter ALL to interact with all animals.");
					keyboard.nextLine();
					stringInput = keyboard.nextLine();
					
					animal = stringToAnimal(stringInput);
					
					//Level 2 - Choice 1: Display information on selected animal(s)
					if (intInput == 1) {
						if (!stringInput.equalsIgnoreCase("ALL")) {
							System.out.println(animal);
						} else {
							for (int i=0; i < animals.length; i++)
							{
								for (int j=0; j < animals[i].length; j++)
								{
									System.out.println(animals[i][j]);
								}
							}
						}
					} 
					
					//Level 2 - Choice 2: Pet selected animal(s)
					else if (intInput == 2) {
						try {
							if (!stringInput.equalsIgnoreCase("ALL")) {
								animal.pet();
							} else {
								for (int i=0; i < animals.length; i++)
								{
									for (int j=0; j < animals[i].length; j++)
									{
										animals[i][j].pet();
									}
								}
							}
						} catch (NullPointerException e) {
							System.out.println("Please enter a valid animal.");
						}
					}
					
					//Level 2 - Choice 3: Develop enrichment
					else if (intInput == 3) {
						System.out.println("What employee would you like to have develop the enrichment(s)?");
						stringInput2 = keyboard.nextLine();
						
						employee = stringToEmployee(stringInput2);
						
						try {
							if (!stringInput.equalsIgnoreCase("ALL")) {
								((Zoologist)employee).developEnrichment(animal);
							} else {
								for (int i=0; i < animals.length; i++)
								{
									for (int j=0; j < animals[i].length; j++)
									{
										((Zoologist)employee).developEnrichment(animals[i][j]);
									}
								}
							}
						} catch (NullPointerException e) {
							System.out.println("Please enter a valid Zoologist.");
						}
					}
					
					//Level 2 - Choice 4: Perform examination
					else if (intInput == 4) {
						System.out.println("What Veterinarian would you like to have perform the examination(s)?");
						stringInput2 = keyboard.nextLine();
						employee = stringToEmployee(stringInput2);
						
						System.out.println("What Vet Tech would you like to have fix any ailments found?");
						stringInput2 = keyboard.nextLine();
						employee2 = stringToEmployee(stringInput2);
						
						try {
							if (!stringInput.equalsIgnoreCase("ALL")) {
								((Vet)employee).physicalExam(animal, (VetTech)employee2);
							} else {
								for (int i=0; i < animals.length; i++)
								{
									for (int j=0; j < animals[i].length; j++)
									{
										((Vet)employee).physicalExam(animals[i][j], (VetTech)employee2);
									}
								}
							}
						} catch (NullPointerException e) {
							System.out.println("Please enter a valid Vetrinarian.");
						}
					}
					
					//Level 2 - Default choice: Incorrect input
					else {
						System.out.println("Please enter a valid input.");
					}
				}//End level 2 prompt
			}//if (intInput == 1)
			
			//Level 1 - Choice 2: Interact with the employees
			else if (intInput == 2) {
				//Begin level 2 prompt
				while(true)
				{
					//Clear the employee object so an employee isn't printed twice in a row by accident
					employee = null;
					
					System.out.println("How would you like to interact with the employees?");
					System.out.println("\tDisplay employee information: 1");
					System.out.println("\tPay employee: 2");
					System.out.println("\tFeed animal using a Zookeeper: 3");
					System.out.println("\tSchedule Guest Services Employees for work using a Guest Services Manager: 4");
					System.out.println("\tSchedule Volunteer for work using a Volunteer Manager: 5");
					System.out.println("\tBack: 0");
					intInput = keyboard.nextInt();
					
					//Level 2 - Choice 0: Back to level 1 prompt 
					if (intInput == 0) {
						break;
					}
					
					//Select what employee to interact with
					System.out.println("What employee would you like to interact with?");
					System.out.println("Enter ALL to interact with all employees.");
					keyboard.nextLine();
					stringInput = keyboard.nextLine();

					employee = stringToEmployee(stringInput);
					
					//Level 2 - Choice 1: Display information on selected employee(s)
					if (intInput == 1) {
						if (!stringInput.equalsIgnoreCase("ALL")) {
							System.out.println(employee);
						} else {
							for (int i=0; i < employees.length; i++)
							{
								System.out.println(employees[i]);
							}
						}
					} 
					
					//Level 2 - Choice 2: Choose employee to pay another employee with
					else if (intInput == 2) {
						
						//Level 3 - Choice 1: Choose which employee is paying
						System.out.println("What Zoo Manager would you like to have pay this employee?");
						stringInput2 = keyboard.nextLine();
						
						employee2 = stringToEmployee(stringInput2);
						
						if (!stringInput.equalsIgnoreCase("ALL")) {
							try {
								((ZooManager)employee2).pay(employee);
							} catch (ClassCastException e) {
								System.out.println("Please select a Zoo Manager.");
							} catch (NullPointerException e) {
								System.out.println("Please select a Zoo Manager.");
							}
						} else {
							try {
								for (int i=0; i < employees.length; i++) 
								{
									((ZooManager)employee2).pay(employees[i]);
								}		
							} catch (ClassCastException e) {
								System.out.println("Please select a valid employee.");
							} catch (NullPointerException e) {
								System.out.println("Please select a valid employee.");
							}
						}
					} 
					
					//Level 2 - Choice 3: Feed animal 
					else if (intInput == 3) {
						System.out.println("What animal would you like to feed?");
						System.out.println("Enter ALL to feed all animals.");
						stringInput = keyboard.nextLine();
						
						if (!stringInput.equalsIgnoreCase("ALL")) {
							animal = stringToAnimal(stringInput);
							
							try {
								((Zookeeper)employee).feedAnimal(animal);
							} catch (ClassCastException e) {
								System.out.println("Please select a Zookeeper.");
							} catch (NullPointerException e) {
								System.out.println("Please select a Zookeeper.");
							}
						} else {
							for (int i=0; i < animals.length; i++)
							{
								for (int j=0; j < animals[i].length; j++) 
								{
									try {
										((Zookeeper)employee).feedAnimal(animals[i][j]);
									} catch (ClassCastException e) {
										System.out.println("Please select a Zookeeper.");
									} catch (NullPointerException e) {
										System.out.println("Please select a Zookeeper.");
									}
								}
							}
						}
					}
					
					//Level 2 - Choice 4: Schedule GSEmployee using GSManager
					else if (intInput == 4) {
						System.out.println("What Guest Services Manager would you like to have schedule this employee?");
						stringInput = keyboard.nextLine();
						
						System.out.println("Where would you like to schedule this employee for?");
						System.out.println("\tThe Gift Shop: 1");
						System.out.println("\tCustomer Service: 2");
						System.out.println("\tTicketing: 3");
						intInput = keyboard.nextInt();
						
						System.out.println("How many hours would you like to schedule this employee for?");
						intInput2 = keyboard.nextInt();
								
						employee2 = stringToEmployee(stringInput);
						try {
							((GSManager)employee2).assignWork((GSEmployee)employee, intInput2, intInput);
						} catch (ClassCastException e) {
							System.out.println("Please select a valid Guest Services Employee/Manager.");
						} catch (NullPointerException e) {
							System.out.println("Please select a valid Guest Services Employee/Manager.");
						}
					}
					
					//Level 2 - Choice 5: Schedule Volunteer using VolManager
					else if (intInput == 5) {
						System.out.println("What Volunteer Manager would you like to have schedule this Volunteer?");
						stringInput = keyboard.nextLine();
						
						System.out.println("Where would you like to schedule this volunteer for?");
						System.out.println("\tMinnesota Trail: 1");
						System.out.println("\tTropics Trail: 2");
						System.out.println("\tRussia's Grizzly Coast: 3");
						System.out.println("\tNorthern Trail: 4");
						System.out.println("\tDiscovery Bay: 5");
						System.out.println("\tFamily Farm: 6");
						System.out.println("\tSouth Entry: 7");
						System.out.println("\tBird Show: 8");
						System.out.println("\tGift Shop: 9");
						System.out.println("\tData Entry: 10");
						intInput = keyboard.nextInt();
						
						System.out.println("How many hours would you like to schedule this volunteer for?");
						intInput2 = keyboard.nextInt();
								
						employee2 = stringToEmployee(stringInput);
						try {
							((VolManager)employee2).assignWork((Volunteer)employee, intInput2, intInput);
						} catch (ClassCastException e) {
							System.out.println("Please select a valid Volunteer/Volunteer Manager.");
						} catch (NullPointerException e) {
							System.out.println("Please select a valid Volunteer/Volunteer Manager.");
						}
					}
					
					//Level 2 - Default choice: Incorrect input
					else {
						System.out.println("Please enter a valid input.");
					}
				}//End level 2 prompt				
			} //else if (intInput == 2)
			
			//Level 1 - Choice 0: End program
			else if (intInput == 0) {
				System.out.println("Ending program...");
				break;
			} 
			
			//Level 1 - Default choice: Incorrect Input
			else { 
				System.out.println("Please enter a valid input.");
			}
		}//End level 1 prompt
		
		keyboard.close();
	}//promptUser()
	
	private static Animal stringToAnimal(String stringInput)
	{
		Animal animal = null;
		
		//Checks which Animal name corresponds to the string that was input by the user,
		//then assigns that Animal to the "animal" variable
		for (int i=0; i < animals.length; i++)
		{
			for (int j=0; j < animals[i].length; j++)
			{
				if (stringInput.equalsIgnoreCase(animals[i][j].getName())) {
					animal = animals[i][j];
				}
			}
		}
		
		return animal;
	}
	
	private static Employee stringToEmployee(String stringInput)
	{
		Employee employee = null;
		
		//Checks which Employee name corresponds to the string that was input by the user,
		//then assigns that Employee to the "employee" variable
		for (int i=0; i < employees.length; i++)
		{
			if (stringInput.equalsIgnoreCase(employees[i].getName())) {
				employee = employees[i];
			}
		}
		
		return employee;
	}
}//class