import java.util.Random;

public class Vet extends AnimalHealth
{
	public Vet()
	{
		
	}//Vet

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay, 
						boolean diagnosis, boolean animalPet)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.pay = pay;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.diagnosis = diagnosis;
		this.animalPet = animalPet;

		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate

	public void physicalExam(Animal animal, VetTech vetTech)
	{
		Random randNum = new Random();
		int randomAilment;

		randomAilment = Math.abs(randNum.nextInt()) % 5  + 1;

		System.out.print(animal.getName() + ": ");
		switch(randomAilment)
		{
			case 1: System.out.println("Healthy/Appears Normal");
				break;
			case 2: System.out.println("Upset Stomach");
				vetTech.fixAilment(animal, randomAilment);
				break;
			case 3: System.out.println("Fever");
				vetTech.fixAilment(animal, randomAilment);
				break;
			case 4: System.out.println("Lack of Appetite");
				vetTech.fixAilment(animal, randomAilment);
				break;
			case 5: System.out.println("Broken Bone");
				vetTech.fixAilment(animal, randomAilment);
				break;
		}
	}
		
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		string+= "Animal diagnosed: " + diagnosis + "\n";
		string+= "Animal pet: " + animalPet + "\n";
		
		return string;
	} //toString
}//class