import java.text.NumberFormat;

public class Employee extends MNZoo
{
	NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
	
	String name;
	int employeeID;
	String title;
	int payType; //1-Salary,2-Hourly,3-Volunteer
	int department;	//1-Animal Control Management, 2-Animal Health, 3-Guest Services
	int employeeStatus;	//1-On Call, 2-Out of Office, 3-On Vacation
	int hoursWorked;
	double pay;

	public Employee()
	{
		name = "Unknown";
		employeeID = 0;
		title = "Unknown";
		payType = 0;
		department = 0;
		employeeStatus = 0;
		hoursWorked = 0;
		pay = 0.00;
	}

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.pay = pay;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate

	//Update employee status
	public void updateEmployeeStatus(int updateInt)
	{
		employeeStatus = updateInt;
		System.out.print("Updated employee status: " + name + " - ");
		switch (employeeStatus)
		{
			case 1: System.out.println("On Call");
				break;
			case 2: System.out.println("Out of Office");
				break;
			case 3: System.out.println("On Vacation");
				break;
			default: System.out.println("Invalid employee status");
				break;
		}
	}

	public String getName() { return name; }
	public int getEmployeeID() { return employeeID; }
	public String getTitle() { return title; }
	public int getPayType() { return payType; }
	public double getPay() { return pay; }
	public int getDepartment() { return department; }
	public int getEmployeeStatus() { return employeeStatus; }
	public int getHoursWorked() { return hoursWorked; }

	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		
		return string;
	} //toString
}//class
