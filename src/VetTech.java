public class VetTech extends AnimalHealth
{
	public VetTech()
	{
		
	}//VetTech

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay, 
						boolean diagnosis, boolean animalPet)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.pay = pay;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.diagnosis = diagnosis;
		this.animalPet = animalPet;

		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public void fixAilment(Animal animal, int randomAilment)
	{
		System.out.print(animal.getName() + " has been ");
		switch(randomAilment)
		{
			case 2: System.out.println("administered TUMS.");
				break;
			case 3: System.out.println("administered a wet towel.");
				break;
			case 4: System.out.println("administered an alternative food source.");
				break;
			case 5: System.out.println("quarantined. (No Strenuous Activity Allowed!)");
				break;
		}
	}//fixAilment
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		string+= "Animal diagnosed: " + diagnosis + "\n";
		string+= "Animal pet: " + animalPet + "\n";
		
		return string;
	} //toString
}//class