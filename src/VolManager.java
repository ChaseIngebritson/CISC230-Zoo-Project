public class VolManager extends GuestServices
{
	public VolManager()
	{
		
	}//VolManager

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.pay = pay;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public void assignWork(Volunteer employee, int numHours, int location)
	{
		System.out.print(employee.getName() + " has been assigned to volunteer for " + numHours + " hours at ");
		switch(location)
		{
			case 1: System.out.println("the Minnesota Trail.");
				break;
			case 2: System.out.println("the Tropics Trail.");
				break;
			case 3: System.out.println("the Russia's Grizzly Coast.");
				break;
			case 4: System.out.println("the Northern Trail.");
				break;
			case 5: System.out.println("the Discovery Bay.");
				break;
			case 6: System.out.println("the Family Farm.");
				break;
			case 7: System.out.println("the South Entry.");
				break;
			case 8: System.out.println("the Bird Show.");
				break;
			case 9: System.out.println("the Gift Shop.");
				break;
			case 10: System.out.println("data entry.");
				break;
		}
		
		employee.work(numHours, location);
	}
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		
		return string;
	} //toString
}//class