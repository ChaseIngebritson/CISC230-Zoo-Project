public class Zookeeper extends ACM
{
	public Zookeeper()
	{
		
	} //ZooKeeper
	
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay, 
								boolean hireStatus, boolean animalFed, boolean researchDone)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.payType = payType;
		this.hireStatus = hireStatus;
		this.animalFed = animalFed;
		this.researchDone = researchDone;
		this.pay = pay;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public void feedAnimal(Animal animal)
	{
		System.out.println(animal.getName() + " has been fed " + animal.getFood() + ".");
	}
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";

		string+= "Employment Status: ";
		if (hireStatus) {
			string+= "Currently Employeed\n";
		} else {
			string+= "Not Currently Employeed\n";
		}

		string+= "Animal Feeding Status: ";
		if (animalFed) {
			string+= "Animal has been fed\n";
		} else {
			string+= "Animal has not been fed\n";
		}

		string+= "Research Status: ";
		if (researchDone) {
			string+= "Research complete\n";
		} else {
			string+= "Research incomplete\n";
		}
		
		return string;
	} //toString
}//class