public class GSEmployee extends GuestServices
{
	int scheduledLocation; //1: Gift Shop, 2: Customer Service, 3: Ticketing
	
	public GSEmployee()
	{
		scheduledLocation = 0;
	}//GSEmployee

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay, 
						int scheduledLocation)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.pay = pay;
		this.scheduledLocation = scheduledLocation;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public void work(int numHours, int location)
	{
		scheduledLocation = location;
		
		System.out.print(name + " has worked for " + numHours + " hours at ");
		switch(location)
		{
			case 1: System.out.println("the gift shop.");
				break;
			case 2: System.out.println("customer service.");
				break;
			case 3: System.out.println("ticketing.");
				break;
			default: System.out.println("unscheduled/unknown location.");
				break;
		}
		
		hoursWorked = hoursWorked + numHours;
		System.out.println("Total hours worked: " + hoursWorked);
	}
	
	public int getScheduledLocation() { return scheduledLocation; }
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		
		string+= "Scheduled location: ";
		switch(scheduledLocation)
		{
			case 1: string+="the Gift Shop\n";
				break;
			case 2: string+="Customer service\n";
				break;
			case 3: string+="Ticketing\n";
				break;
			default: string+="Unscheduled/Unknown location\n";
				break;
		}
		
		return string;
	} //toString
}//class