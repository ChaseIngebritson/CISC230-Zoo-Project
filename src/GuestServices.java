public class GuestServices extends Employee
{
	public GuestServices()
	{

	}//GSEmployee

	//Populate the employee
	public void populate(String name, int employeeID, String title, int payType, int department, int employeeStatus, int hoursWorked, double pay)
	{
		this.name = name;
		this.employeeID = employeeID;
		this.title = title;
		this.payType = payType;
		this.department = department;
		this.employeeStatus = employeeStatus;
		this.hoursWorked = hoursWorked;
		this.pay = pay;
		
		System.out.println("(" + title + ") " + name + " has been populated.");
	} //populate
	
	public String toString()
	{
		String string = "";

		string+= "Employee Name: " + name + "\n";
		string+= "Employee ID: " + employeeID + "\n";
		string+= "Job Title: " + title + "\n";
		
		string+= "Pay Type: ";
		switch (payType)
		{
			case 1: string+= "Salaried\n";
				break;
			case 2: string+= "Hourly\n";
				break;
			case 3: string+= "Volunteer\n";
				break;
		}
		
		string+= "Pay amount: " + currencyFormatter.format(pay) + "\n";
		
		string+= "Department: ";
		switch (department)
		{
			case 1: string+= "Animal Collection Management\n";
				break;
			case 2: string+= "Animal Health\n";
				break;
			case 3: string+= "Guest Services\n";
				break;
		}

		string+= "Employee Status: ";
		switch (employeeStatus)
		{
			case 1: string+= "On Call\n";
				break;
			case 2: string+= "Out of Office\n";
				break;
			case 3: string+= "On Vacation\n";
				break;
		}
		
		string+= "Hours worked: " + hoursWorked + "\n";
		
		return string;
	} //toString
}//class